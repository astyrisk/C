/* convert chars to lower case */

#include <stdio.h>
#include <math.h>

int lower(int c);

int main(){

    int c = 'M';
    double m = c;
    printf("%c", (int) sqrt(c));

    return 0;
}


int lower(int c){
    if( c >= 'A' && c <= 'Z')
        return c + 'a' - 'A';
    else 
        return c;
}
