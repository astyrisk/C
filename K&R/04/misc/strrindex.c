#include <stdio.h>
#include <string.h>

#define MAXLINE 1000

int getln(char line[], int max);
int strrindex(char line[], char to_find);

char ch = 'x';


int main()
{

    char line[MAXLINE];

    while(getln(line, MAXLINE) > 0)
        printf("%d\n", strrindex(line, ch));

    return 0;
}

int getln(char s[], int lim)
{
    int c,i;

    i=0;
    while(--lim > 0 && (c = getchar()) != EOF && c != '\n' )
        s[i++] = c;
    if(c == '\n')
        s[i++] = c;
    s[i] = '\0';
    return i;
}

int strrindex(char* s, char c)
{
    int i;
    for(i=strlen(s)-1; (s[i] != c) && (i >= 0); i--) // reversed
        ;
    return (i) ? i : -1;
}


