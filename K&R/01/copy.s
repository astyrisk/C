.text
.globl main
main:
	pushq %rbp
	movq %rsp, %rbp
.Lbb1:
	callq getchar
	movl %eax, %edi
	cmpl $-1, %edi
	jz .Lbb3
	callq putchar
	jmp .Lbb1
.Lbb3:
	movl $0, %eax
	leave
	ret
.type main, @function
.size main, .-main
/* end function main */

.section .note.GNU-stack,"",@progbits

