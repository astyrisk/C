#include <stdio.h>
#define IN 1
#define OUT 0

int line() {
    int c, nl;
    nl = 0;
    while((c = getchar()) != EOF )
        if ( c == '\n')
            ++nl;
    return nl;
}

int chars(){
    long nc;
    nc = 0;
    while(getchar()!= EOF)
        ++nc;
    return nc;
}

int chars2() {
    int nc;
    for( nc=0; getchar() != EOF; ++nc)
        ;
    return nc;
}

void word(){
    int c, nl, nw , nc, state;
    state = OUT;
    nl = nw = nc = 0;
    while((c=getchar()) != EOF){
        ++nc;
        if( c == '\n')
            ++nl;
        if( c == ' ' || c == '\n' || c == '\t')
            state = OUT;
        else if (state == OUT){ 
            state = IN;
            ++nw;
        }
    }
    printf("%d %d %d\n", nl, nw, nc);
}

void arr(){
    int c, i, nwhite, nother;
    int ndigit[10];
    nwhite = nother=0;
    for(i=0; i< 10; ++i)
        ndigit[i] = 0;

    while((c = getchar()) != EOF)
        if(c >= '0' && c<= '9')
            ++ndigit[c-'0'];
        else if( c == ' ' || c == '\n' || c == '\t')
            ++nwhite;
        else
            ++nother;
    printf("digits = ");
    for(i=0; i<10; ++i)
        printf("%d", ndigit[i]);
    printf(", white space %d, other = %d\n", nwhite, nother);
}

int main() { 

    arr();

    return 0;
}
