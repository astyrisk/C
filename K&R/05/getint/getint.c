#include <stdio.h>
#include <ctype.h>

#define BUFSIZE 10


int getch(void);
void ungetch(int);

int getint(int *pn) 
{
    int c, sign;

    while(isspace(c = getch())) /* skip white spaces */
        ;
    if(!isdigit(c) && c != EOF && c != '+' && c != '-'){
        ungetch(c);
        return 0;
    }
    sign = (c == '-') ? -1 : 1;
    if (c == '+' || c == '-'){
        c = getch();
        if(!isdigit(c)){
            ungetch(sign == 1 ? '+' : '-');
            return 0;
        }
    }
    for(*pn = 0; isdigit(c); c = getch())
        *pn = 10 * *pn + (c - '0');
    *pn *= sign;
    if(c != EOF)
        ungetch(c);
    return c;
}

int main()
{
    int n, array[BUFSIZE], getint(int *);

    for(n = 0; n < BUFSIZE && getint(&array[n]) != EOF; n++)
        ;
    for(n=0; n < BUFSIZE && printf("%d\n", array[n]); n++ )
        ;
    return 0;
}

