/*
  5-2: floating-point analog of getint
*/

#include <stdio.h>
#include <ctype.h>

#define SIZE 10

int getch(void);
void ungetch(int);


double getfloat(double *pn)
{
    int c, sign, nextc, power = 1;

    while(isspace(c = getch())); /* skip white spacces */

    if (!isdigit(c) && c != EOF && c != '-' && c != '+' && c !='.'){
        ungetch(c);
        return 0;
    }
    sign = (c == '-') ? -1 : 1;
    if(c == '+' || c == '-'){
        nextc = getch();
        if(isdigit(nextc))
            c = nextc;
        else{
            ungetch(nextc);
            ungetch(c);
            return 0;
        }
    }
    for(*pn = 0; isdigit(c); c = getch())
        *pn = 10.0 * *pn + (c - '0');
    if(c == '.')
        getch();
    for(; isdigit(c); c = getch()){
        *pn = 10.0 * *pn + (c - '0');
        power = power * 10.0;
    }
    *pn = sign * *pn / power;
    if (c != EOF)
        ungetch(c);
    return c;
}


int main()
{
    int n;
    double arr[SIZE], getfloat(double *);

    for(n = 0; n < SIZE && getfloat(&arr[n]) != EOF; n++)
        ;
    for(n = 0; n < SIZE && printf("%g", arr[n]) != EOF; n++)
        ;
    printf("\n");
    

    return 0;
}
