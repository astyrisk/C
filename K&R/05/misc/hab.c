/* playing around with pointers */
#include <stdio.h>


/* prints: print a string */
void prints(char *c)
{
    while(*c != '\0' && putchar(*c++)) 
        ;
}

void test01()
{
    int x = 1, y = 2, z[10];
    int *ip;

    ip = &x; /* ip points to x */
    y = *ip; /* y == 1 */
    *ip = 0; /* x == 0 */
    ip = &z[0]; /* ip now points to z[0] */
}

void test02(int l)
{
    printf("%d\n", l);
    int x;
    int *px;
    printf("%i\n", px);
    printf("%i\n", (px+1));

}

void test03()
{
    char s[] = "lmao"; /* chars array */
    char *c;
    c = s;
    printf("%c\n",  *(c+1));
   *s = 'k'; 
   printf("%s\n", s);
}


int main()
{
    test03();
    return 0;
}

