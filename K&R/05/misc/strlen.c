#include <stdio.h>

int strlen(char *);

int main()
{
    char *s = "lmao?";
    printf("%d\n", strlen("kek"));
    printf("%d\n", strlen(s)); 
    printf("%d\n", strlen(s+1));  /* begins from (s+1) */
    return 0;
}

int strlen(char *s)
{
    int n;
    
    for(n = 0; *s != '\0'; s++)
        n++;
    return n;
}
