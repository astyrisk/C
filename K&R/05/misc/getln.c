#include <stdio.h>

/* getln: get line into s, return length */
int getln(char s[], int lim)
{
    int c, i;

    i = 0;
    while (--lim > 0&& (c = getchar()) != EOF && c != '\n')
        s[i++] = c;
    if (c == '\n') /* line's end */
        s[i++] = c;
    s[i] = '\0'; /* string's end */
    return i;
}
