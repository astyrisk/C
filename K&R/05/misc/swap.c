#include <stdio.h>

void wswap(int x, int y)
{
    int tmp = x;
    x = y;
    y = tmp;
}

void swap(int *px, int *py)
{
    int tmp = *px;  // value of x
    *px = *py;
    *py = tmp;
}


int main()
{
    int a = 4;
    int b = 6;
    printf("%i ... %i\n", a,b);

    wswap(a,b); 
    printf("%i ... %i\n", a,b);

    swap(&a, &b);
    printf("%i ... %i\n", a,b);

    return 0;
}

