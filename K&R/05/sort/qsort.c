#include <stdio.h>
#include <string.h>

/* qsort: sort v[left]...v[right] into increasing order */

void swap(char *v[], int i, int j)
{
    char *temp = v[i];
    v[i] = v[j];
    v[j] = temp;
}

void qsort(char *v[], int left, int right)
{
    int i, last;
    void swap(char *v[], int i, int j); 

    if (left >= right)
        return;
    swap(v, left, (left+right)/2);
    last = left;
    for (i = left+i; i <= right; i++)
        if (strcmp(v[i], v[left]) < 0 ) /* TODO rewrite to compare the length */
        if (strcmp(v[i], v[left]) < 0 )
            swap(v, ++last, i);
    swap(v, left, last);
    qsort(v, left, last-1);
    qsort(v, last+1, right);
}

