#include <stdio.h>
#define ALLOCSIZE 10000

static char allocbuf[ALLOCSIZE];
static char *allocp = allocbuf; /* next free position */

char *alloc(int n)
{
    if(allocbuf + ALLOCSIZE - allocp >= n){ /* it fits */
        allocp += n;
        return allocp - n; 
    } else 
        return 0;
}

void afree(char *p)
{
    if(p >= allocbuf && p < allocbuf + ALLOCSIZE)
        allocp = p;
}

int strlen(char *s)
{
    char *p = s;

    while (*p != '\0')
        p++;
    return p - s;
}

int main()
{
    
    printf("%i", strlen("king"));

    return 0;
}

